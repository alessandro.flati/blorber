import json


class Stats:
    def __init__(self, stats_config):
        self.__stats = stats_config

    def get_dict_stats(self):
        return self.__stats

    def get_multipliers(self):
        multipliers = {
            "fire": 1,
            "water": 1,
            "wind": 1,
            "earth": 1,
        }

        for element in multipliers.keys():
            multipliers[element] = self.__get_current_multiplier_for_element(element)
            multipliers[element] *= self.__collect_blorb_multipliers(element)

        return multipliers

    def __get_current_multiplier_for_element(self, element):
        additional_dmg = 100
        additional_dmg += self.__stats['skill_proficiency_%']
        additional_dmg += self.__stats[element]['companion_%']
        additional_dmg += self.__stats[element]['understanding_%']
        additional_dmg += self.__stats[element]['relic_%']
        additional_dmg += self.__stats[element]['blorb']['global_%']
        return additional_dmg / 100

    def __collect_blorb_multipliers(self, element):
        multiplier = 100
        for p in ['wind', 'fire', 'water', 'earth']:
            for x in self.__stats[p]['blorb']['slots']:
                if x['type'] == 'all':
                    multiplier += x['value']
                elif x['type'] == element:
                    multiplier += x['value'] * (2. if p == element else 1.)
        return multiplier / 100


if __name__ == '__main__':
    # Load the stats from the file
    with open('stats.json', 'r') as f:
        stats = Stats(json.load(f))
    print(stats.get_multipliers())