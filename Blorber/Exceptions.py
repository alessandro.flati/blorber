class GlobalPercentExtractionError(Exception):
    pass

class InvalidGlobalPercentError(Exception):
    def __init__(self, message):
        self.message = message

class AbortSelection(Exception):
    pass
