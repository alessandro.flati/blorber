elem_color = {
    "wind": "green",
    "earth": "yellow",
    "fire": "red",
    "water": "blue",
    "all": "light_cyan"
}

rarity_color = {
    2: "white",
    3: "green",
    4: "yellow",
    5: "light_magenta",
    6: "red",
    7: "light_cyan"
}
