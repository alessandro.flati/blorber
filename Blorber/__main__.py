import copy
import json
import os
import pathlib
import re
import subprocess
import sys
import time

import colorama
import cv2
import keyboard
import numpy as np
from termcolor import colored

project_root = pathlib.Path(__file__).parent / '..'
sys.path.append(str(project_root.resolve()))
sys.path.append(str(project_root / 'resources' / 'platform-tools'))
os.environ['PATH'] += os.pathsep + str(project_root / 'resources' / 'platform-tools')

from Blorber.Colors import elem_color, rarity_color
from Blorber.Exceptions import GlobalPercentExtractionError, InvalidGlobalPercentError, AbortSelection
from Blorber.Stats import Stats
from InferenceEngine.InferenceEngine import InferenceEngine


class Blorber:
    def __init__(self):
        with open('config.json', 'r') as config_file:
            self.__config = json.load(config_file)
        with open('stats.json', 'r') as stats_file:
            self.__config['stats'] = json.load(stats_file)
            if 'blorb_level' not in self.__config['stats']:
                blorb_level = input("Please enter your blorb level. This will be asked only once.\n")
                self.__config['stats']['blorb_level'] = int(blorb_level)
                with open('stats.json', 'w') as f:
                    json.dump(self.__config['stats'], f, indent=4)

        # Connect to Bluestacks
        if 'bluestacks_adb_port' in self.__config:
            bluestacks_adb_port = self.__config['bluestacks_adb_port']
        elif 'window_title' in self.__config:
            bluestacks_adb_port = self.__find_bluestacks_adb_port(self.__config['window_title'])
        else:
            print("Please edit config.json and add the window_title of the Bluestacks instance you want to use, "
                  "or the bluestacks_adb_port of the instance.")
            exit(1)
        self.__bluestacks_uri = f"127.0.0.1:{bluestacks_adb_port}"
        subprocess.run(["adb", "disconnect", self.__bluestacks_uri], check=False)
        subprocess.run(["adb", "connect", self.__bluestacks_uri], check=True)

        self.__current_stats = Stats(self.__config['stats'])
        self.__weights = self.__config['elemental_weights']
        self.__confirm_click_delay = self.__config['confirm_click_delay']
        self.__blorb_click_delay = self.__config['blorb_click_delay']

        self.__data_collection_mode = self.__config['data_collection_mode']
        # Create Trainer/resources directory if it doesn't exist
        if self.__data_collection_mode:
            os.makedirs('Trainer/resources', exist_ok=True)

        self.__new_flag_png = cv2.imread('resources/new.png')
        self.__inference_engine = InferenceEngine()

        # Utilize the delay introduced by the InferenceEngine() to prevent device offline error
        wm_size_output = subprocess.check_output(f"adb -s {self.__bluestacks_uri} shell wm size")
        resolution = re.findall(r'\d+x\d+', wm_size_output.decode())[0]
        assert resolution == "1080x1920", f"Display resolution must be 1080x1920, your resolution is {resolution}"

    def main_loop(self):
        self.__tap('summon_blorb')
        while True:
            try:
                full_res_image = self.__screenshot()
                if not self.__is_new_flag_in_screenshot(full_res_image):
                    continue
                # Wait to ensure the flashbang is faded in completely
                time.sleep(0.1)
                full_res_image = self.__screenshot()
            except subprocess.CalledProcessError:
                print('Error capturing screenshot, retrying...')
                continue

            full_res_image, inferred_data = self.__try_to_extract_global_percent(full_res_image)
            if inferred_data is None:
                continue

            self.__print_inferred_data(inferred_data)

            if self.__data_collection_mode:
                self.__handle_data_collection(inferred_data, full_res_image)
            else:
                self.__handle_inferred_data(inferred_data)

            self.__restart_blorb()

            # Print a dashed line to separate the iterations
            print('=' * 74)

    def __try_to_extract_global_percent(self, full_res_image):
        percent_retries = 0
        while True:
            if percent_retries == 5:
                print(f"Failed to extract global percent for {percent_retries} times, keeping left choice as "
                      f"a preemptive measure.")
                self.__tap("left_blorb")
                self.__restart_blorb()
                # Print a dashed line to separate the iterations
                print('=' * 74)
                break
            try:
                inferred_data = self.__inference_engine.infer(full_res_image)
                break
            except GlobalPercentExtractionError:
                print('Error extracting global percent, retrying...')
                full_res_image = self.__screenshot()
                percent_retries += 1
                continue
            except InvalidGlobalPercentError as err:
                print(err.message)
                full_res_image = self.__screenshot()
                percent_retries += 1
                continue
        return full_res_image, inferred_data if percent_retries < 5 else None

    def __screenshot(self):
        proc = subprocess.Popen(["adb", "-s", self.__bluestacks_uri, "shell", "screencap", "-p"],
                                stdout=subprocess.PIPE,
                                stderr=subprocess.DEVNULL)
        output, _ = proc.communicate()
        output = output.replace(b'\r\n', b'\n')

        ss_arr = np.frombuffer(output, np.uint8)
        try:
            image = cv2.imdecode(ss_arr, cv2.IMREAD_COLOR)
        except cv2.error:
            print('Error decoding screenshot, retrying...')
            return self.__screenshot()

        return image

    def __handle_inferred_data(self, inferred_data):
        raw_right_stats = self.__get_new_stats_from_inferred_data(inferred_data)
        right_stats = Stats(raw_right_stats)
        current_multipliers = self.__current_stats.get_multipliers()
        right_multipliers = right_stats.get_multipliers()

        # Print found blorb data
        self.__print_old_and_new_multipliers(current_multipliers, right_multipliers)

        is_better = self.__is_new_blorb_better(current_multipliers, right_multipliers)

        if is_better:
            print(colored('Better blorb found', "green"))
        else:
            print(colored('Worse blorb found', "red"))
        print()

        if self.__config[f'pause_on_{"better" if is_better else "worse"}_blorb']:
            try:
                if input('Press Enter to continue, write anything to abort: ') == "":
                    pass
                else:
                    print("Aborting!")
                    raise AbortSelection

            # Invert actions if aborting
            except (KeyboardInterrupt, AbortSelection):
                is_better = not is_better

        if is_better:
            self.__tap("right_blorb")
            self.__save_new_stats(raw_right_stats)
        else:
            self.__tap("left_blorb")

    def __handle_data_collection(self, inferred_data, full_res_image):
        screenshot_hash = int(time.time())
        print('Press enter to confirm the inferred data or n/N to mark it as incorrect')
        choice = None
        validated = False
        while choice not in ['n', 'N', '']:
            choice = input()
        if choice == '':
            validated = True

        cv2.imwrite(f'Trainer/resources/{screenshot_hash}.png', full_res_image)
        self.__annotate_data(screenshot_hash, inferred_data, validated)
        print(
            "Press left arrow key to choose left blorb, or manually select right blorb then press left arrow key")
        while True:
            if keyboard.is_pressed('left'):
                while keyboard.is_pressed('left'):
                    pass
                self.__tap('left_blorb')
                break

    def __annotate_data(self, img_hash, inferred_data, validated):
        try:
            with open('Trainer/blorbAnnotations.json', 'r') as f:
                annotations = json.load(f)
        except FileNotFoundError:
            annotations = {}
        if img_hash not in annotations:
            annotations[img_hash] = {}
        annotations[img_hash]['global'] = {
            'type': inferred_data['global']['type'],
            'percent': inferred_data['global']['percent']
        }
        annotations[img_hash]['first_slot'] = {
            'type': inferred_data['first_slot']['type'],
            'percent': inferred_data['first_slot']['percent']
        }
        annotations[img_hash]['second_slot'] = {
            'type': inferred_data['second_slot']['type'],
            'percent': inferred_data['second_slot']['percent']
        }
        annotations[img_hash]['third_slot'] = {
            'type': inferred_data['third_slot']['type'],
            'percent': inferred_data['third_slot']['percent']
        }
        annotations[img_hash]['fourth_slot'] = {
            'type': inferred_data['fourth_slot']['type'],
            'percent': inferred_data['fourth_slot']['percent']
        }
        annotations[img_hash]['validated'] = validated
        with open('Trainer/blorbAnnotations.json', 'w') as f:
            json.dump(annotations, f, indent=4)

    def __get_new_stats_from_inferred_data(self, results):
        new_stats = copy.deepcopy(self.__config['stats'])
        new_blorb_type = results['global']['type']
        new_stats[new_blorb_type]['blorb'] = {
            'global_%': results['global']['percent'],
            'slots': [
                {
                    'type': results['first_slot']['type'],
                    'value': results['first_slot']['percent'],
                },
                {
                    'type': results['second_slot']['type'],
                    'value': results['second_slot']['percent'],
                },
                {
                    'type': results['third_slot']['type'],
                    'value': results['third_slot']['percent'],
                },
                {
                    'type': results['fourth_slot']['type'],
                    'value': results['fourth_slot']['percent'],
                },
            ]
        }
        return new_stats

    def __is_new_blorb_better(self, current_multipliers, right_multipliers):
        normal_sum_of_current_multipliers = sum(current_multipliers.values())
        normal_sum_of_right_multipliers = sum(right_multipliers.values())
        weighted_sum_of_current_multipliers = sum([current_multipliers[element] * self.__weights[element]
                                                   for element in self.__weights])
        weighted_sum_of_right_multipliers = sum([right_multipliers[element] * self.__weights[element]
                                                 for element in self.__weights])
        new_blorb_is_better = (
                weighted_sum_of_right_multipliers > weighted_sum_of_current_multipliers or (
                weighted_sum_of_right_multipliers == weighted_sum_of_current_multipliers and
                normal_sum_of_right_multipliers > normal_sum_of_current_multipliers
        )
        )
        return new_blorb_is_better

    def __save_new_stats(self, new_stats):
        self.__current_stats = Stats(new_stats)
        self.__config['stats'] = self.__current_stats.get_dict_stats()
        with open('stats.json', 'w') as f:
            json.dump(self.__config['stats'], f, indent=4)

    @staticmethod
    def __print_inferred_data(results):
        print('Data extracted:')
        print()
        g_type = results["global"]["type"]
        for key, value in results.items():
            t = value['type']
            p = value['percent']
            p_str = str(p) + "%"
            if t == g_type and p < 10:
                p_str += " (x2)"
            extracted = f"{colored(t + ':', elem_color.get(t, 'white')):<15} {colored(p_str, rarity_color.get(p, 'white'))}"
            print(f"{extracted}")
        print()

    @staticmethod
    def __print_old_and_new_multipliers(current_multipliers, right_multipliers):
        # Print in a tabular fashion
        for elem in ['wind', 'earth', 'fire', 'water']:
            l, r = current_multipliers[elem], right_multipliers[elem]
            mult = r / l
            mult_str = colored(f"{(r / l):2.2f}x", "green" if mult > 1 else "red")
            print(f'{colored(elem + ":", elem_color[elem]):<15} {mult_str} ( {l:5.1f} -> {r:5.1f} )')
        print()

    def __tap(self, button_name):
        # Hardcode coordinates under 1080x1920 resolution
        match button_name:
            case 'summon_blorb':
                x, y = 540, 1350
            case 'left_blorb':
                x, y = 290, 1560
            case 'right_blorb':
                x, y = 790, 1560
            case 'confirm':
                x, y = 390, 1320
            case _:
                raise NotImplementedError

        subprocess.run(f"adb -s {self.__bluestacks_uri} shell input tap {x} {y}")

    def __restart_blorb(self):
        time.sleep(self.__confirm_click_delay)
        self.__tap('confirm')
        time.sleep(self.__blorb_click_delay)
        self.__tap('summon_blorb')

    def __is_new_flag_in_screenshot(self, full_res_image):
        cropped_image = full_res_image[519:519 + 28, 748:748 + 76]
        # Check if self.__new_flag_png is exactly equal to the cropped image
        return np.array_equal(cropped_image, self.__new_flag_png)

    @staticmethod
    def __find_bluestacks_adb_port(window_title):
        bluestacks_conf_path = 'C:\\ProgramData\\BlueStacks_nxt\\bluestacks.conf'
        with open(bluestacks_conf_path, 'r') as f:
            conf_lines = [line for line in f]
        # Find device name
        matches1 = (re.match(rf'bst\.instance\.(\w+)\.display_name="{window_title}"', line, flags=re.IGNORECASE)
                    for line in conf_lines)
        try:
            display_name_match = next(filter(None, matches1))
        except StopIteration:
            print(f"Window title {window_title} not found.")
            exit(1)
        device_name = display_name_match.group(1)
        # Find device adb port
        matches2 = (re.match(rf'bst\.instance\.{device_name}\.status\.adb_port="(\d+)"', line)
                    for line in conf_lines)
        adb_port_match = next(filter(None, matches2))
        adb_port = int(adb_port_match.group(1))

        return adb_port


def main():
    os.chdir(project_root)
    colorama.init()
    blorber = Blorber()
    blorber.main_loop()


if __name__ == '__main__':
    main()
