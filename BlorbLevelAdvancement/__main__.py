import json


class BlorbLevelAdvancementEngine:
    def __init__(self):
        self.__exponents = {'A': 3, 'B': 6, 'C': 9, 'D': 12, 'E': 15, 'F': 18, 'G': 21, 'H': 24, 'I': 27, 'J': 30,
                            'K': 33, 'L': 36, 'M': 39, 'N': 42, 'O': 45, 'P': 48, 'Q': 51, 'R': 54, 'S': 57, 'T': 60}
        with open('blorb_level.json', 'r') as f:
            parsed_json = json.load(f)
            self.__current_level = parsed_json['level']
            self.__current_mana = self.__parse_mana(parsed_json['mana'])
            self.__max_stage = parsed_json['max_stage']
            self.__max_stage -= self.__max_stage % 20
        with open('resources/blorb_level_advancement.json', 'r') as f:
            parsed_json = json.load(f)
            self.__mana_required_for_level = parsed_json['mana_required_for_level']
            self.__mana_required_for_level = [self.__parse_mana(x) for x in self.__mana_required_for_level]
            self.__mana_required_for_summoning = parsed_json['mana_required_for_summoning']
            self.__mana_required_for_summoning = [self.__parse_mana(x) for x in self.__mana_required_for_summoning]
            self.__effects = parsed_json['effects']
            self.__minmax_percents = parsed_json['minmax_percents_for_level']
        self.__level = self.__current_level
        self.__mana = self.__current_mana

        self.__progress = [
            {
                'day': 0,
                'level': self.__level,
                'mana': self.__mana
            }
        ]

    def __level_up(self):
        self.__mana -= self.__mana_required_for_level[self.__level]
        self.__level += 1

    def __get_daily_mana(self):
        return 3 * self.__mana_gain_for_max_stage(self.__max_stage)

    def __parse_mana(self, mana):
        try:
            mult = float(mana)
        except ValueError:
            mult = (10 ** self.__exponents[mana[-1]])
            mult *= float(mana[:-1])
        return mult

    @staticmethod
    def __mana_gain_for_max_stage(max_stage):
        return 3.17594e-9 * (3 ** (max_stage / 20))

    def start_simulation(self):
        i = 1
        while True:
            self.__mana += self.__get_daily_mana()
            if self.__mana >= self.__mana_required_for_level[self.__level]:
                self.__level_up()
            else:
                self.__progress.append({
                    'day': i,
                    'level': self.__level,
                    'mana': self.__mana
                })
                break
            self.__progress.append({
                'day': i,
                'level': self.__level,
                'mana': self.__mana
            })
            i += 1

    def __unparse_mana(self, mana):
        if mana < 1e3:
            return f"{mana:.2f}"
        for label, exponent in self.__exponents.items():
            if mana < 10 ** (exponent + 3):
                return f"{mana / 10 ** exponent:.2f}{label}"

    def print_results(self):
        print(f"If you just consider level up, you will reach level {self.__progress[-2]['level']} in "
              f"{self.__progress[-2]['day']} days, with {self.__unparse_mana(self.__progress[-2]['mana'])} mana left.")
        print(f"Up until that day, you can roll the following number of times: ")
        # Display in tabular form
        print(f"{'Day':<5}{'Level':<10}{'Effect':<20}{'Rolls left':<15}{'Daily rolls':<15}{'Max top stat':<15}")
        for i in range(0, self.__progress[-1]['day']):
            mana_required = self.__mana_required_for_summoning[self.__progress[i]['level']]
            daily_mana = self.__get_daily_mana()
            number_of_rolls = int(self.__progress[i]['mana'] / mana_required)
            number_of_daily_rolls = int(daily_mana / mana_required)
            effect = f"{self.__effects[self.__progress[i]['level'] - 1]['type']} +{self.__effects[self.__progress[i]['level'] - 1]['percent']}%"
            max_top_stat = '~' + str(round(self.__minmax_percents[self.__progress[i]['level'] - 1]['max'])) + '%'
            print(f"{self.__progress[i]['day']:<5}{self.__progress[i]['level']:<10}{effect:<20}{number_of_rolls:<15}"
                  f"{number_of_daily_rolls:<15}{max_top_stat:<15}")


if __name__ == '__main__':
    engine = BlorbLevelAdvancementEngine()
    engine.start_simulation()
    engine.print_results()
