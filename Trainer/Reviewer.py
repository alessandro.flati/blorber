import copy
import json
import os
import time
import keyboard
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import matplotlib
matplotlib.use('TkAgg')
plt.ion()  # Turn on interactive mode


class Annotator:
    def __init__(self, img_paths):
        self.__img_paths = os.listdir(img_paths)
        self.__save_path = 'blorbAnnotations.json'
        self.__annotations = {}

    def save_annotations(self):
        # Save annotations to file as JSON
        with open(self.__save_path, 'w') as f:
            f.write(json.dumps(self.__annotations, indent=4))
        print(f'Annotations saved to {self.__save_path}')

    def load_annotations(self):
        # Load annotations from file
        with open(self.__save_path, 'r') as f:
            self.__annotations = json.loads(f.read())
        print(f'Annotations loaded from {self.__save_path}')

    def start_annotation_process(self):
        print('Starting annotation process')
        for img_path, annotation in copy.deepcopy(self.__annotations).items():
            if 'validated' in annotation and annotation['validated']:
                continue
            self.annotate_image(img_path, annotation)
            self.save_annotations()

        print('Annotation process complete')

    def annotate_image(self, img_path, annotation):

        # Display the image on screen
        try:
            img = mpimg.imread(f'resources/{img_path}.png')
        except FileNotFoundError:
            print(f"File not found: {img_path}")
            del self.__annotations[img_path]
            self.save_annotations()
            return
        print(f"Annotating {img_path}")
        plt.imshow(img)
        plt.draw()
        mng = plt.get_current_fig_manager()
        mng.window.state('zoomed')  # For TkAgg backend; might vary for others
        time.sleep(0.5)
        plt.pause(0.001)

        for k, v in annotation.items():
            if k == 'validated':
                continue
            print(f"{k}")
            for k2, v2 in v.items():
                print(f"  {k2}: {v2}")
                # Ask if it's correct
                print("Is this correct? (Y/n): ", end='', flush=True)
                while True:
                    answer = input()
                    if answer == 'y' or answer == 'Y' or answer == '':
                        break
                    elif answer == 'n' or answer == 'N':
                        # Ask for the correct value
                        print(f"Input the correct {k2}: ", end='', flush=True)
                        correct_value = input()
                        self.__annotations[img_path][k][k2] = correct_value
                        break
                    time.sleep(0.1)

        self.__annotations[img_path]['validated'] = True
        plt.close('all')


def main():
    annotator = Annotator('resources')
    annotator.load_annotations()
    annotator.start_annotation_process()


if __name__ == '__main__':
    main()
