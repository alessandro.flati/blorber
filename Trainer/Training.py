import json
import os
import time

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from keras.src.callbacks import EarlyStopping, ReduceLROnPlateau, ModelCheckpoint
from sklearn.model_selection import train_test_split
from tensorflow.keras.layers import Input, Conv2D, MaxPooling2D, Flatten, Dense, Dropout
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import Adam

matplotlib.use('TkAgg')


def convert_type_to_int(blorb_type):
    if blorb_type == 'fire':
        return 0
    elif blorb_type == 'earth':
        return 1
    elif blorb_type == 'wind':
        return 2
    elif blorb_type == 'water':
        return 3
    elif blorb_type == 'all':
        return 4
    else:
        raise ValueError(f'Unknown type: {blorb_type}')


def convert_int_to_type(type_int):
    types = ['fire', 'earth', 'wind', 'water', 'all']
    if type_int < len(types):
        return types[type_int]
    else:
        return 'Unknown'


class Trainer:
    def __init__(self, slot_no, prediction_type, example=False):
        self.__slot_no = slot_no  # 'global', 'first', 'second', 'third', 'fourth'
        self.__prediction_type = prediction_type  # 'type', 'percent'
        self.__example = example
        self.__checkpoint_dir = 'checkpoints'

        with open('blorbAnnotations.json', 'r') as f:
            self.__annotations = json.load(f)

        if slot_no == 'global':
            self.__checkpoint_filepath = os.path.join(self.__checkpoint_dir, f'global_{prediction_type}.keras')
        else:
            self.__checkpoint_filepath = os.path.join(self.__checkpoint_dir, f'{slot_no}_slot_{prediction_type}.keras')

        self.__channels = 3
        if self.__prediction_type == 'type':
            self.__num_types = 5
        else:
            self.__num_types = 6

        self.__searching_points_patience = 25
        self.__searching_points_epochs = 100
        self.__initial_lr_for_searching = 1e-4
        self.__minimum_lr = 1e-4
        self.__target_loss = 1e-5
        self.__initial_lr = 1e-3
        self.__current_lr = self.__initial_lr
        self.__current_lr_decay = 0.9

        self.__epochs = 10000
        self.__batch_size = 64

        self.__x_data = []
        self.__y_data = []

        tf.random.set_seed(time.time())

        self.__fill_dataset()

        self.__model = self.__create_model()

        self.__early_stopping_for_searching = EarlyStopping(monitor='val_loss', patience=5, verbose=1, mode='min',
                                                            restore_best_weights=True)
        self.__early_stopping = EarlyStopping(monitor='val_loss', patience=10, verbose=1, mode='min',
                                              restore_best_weights=True)
        self.__early_stopping_for_loss = EarlyStopping(monitor='loss', patience=25, verbose=1, mode='min',
                                                       restore_best_weights=True)

        self.__model_checkpoint_callback = ModelCheckpoint(
            filepath=self.__checkpoint_filepath,
            save_weights_only=False,
            monitor='val_loss',
            mode='min',
            save_best_only=True,
            verbose=1)

        self.__model_checkpoint_callback_for_loss = ModelCheckpoint(
            filepath=self.__checkpoint_filepath,
            save_weights_only=False,
            monitor='loss',
            mode='min',
            save_best_only=True,
            verbose=1)

        self.__reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.9, patience=25, min_lr=self.__minimum_lr,
                                             verbose=1)
        self.__reduce_lr_for_loss = ReduceLROnPlateau(monitor='loss', factor=0.9, patience=25, min_lr=self.__minimum_lr,
                                                      verbose=1)

    def __load_and_preprocess_image(self, image_path):
        image = tf.io.read_file(image_path)

        image = tf.image.decode_png(image, channels=self.__channels, dtype=tf.uint8)

        if self.__slot_no == 'global':
            image = image[930:1000, 568:1008]  # 1008 - 568 = 440, 1000 - 930 = 70
            self.__image_width = 440
            self.__image_height = 70
        elif self.__slot_no == 'first':
            image = image[1050:1100, 588:1008]  # 1008 - 588 = 420, 1100 - 1050 = 50
            self.__image_width = 420
            self.__image_height = 50
        elif self.__slot_no == 'second':
            image = image[1130:1180, 588:1008]
            self.__image_width = 420
            self.__image_height = 50
        elif self.__slot_no == 'third':
            image = image[1210:1260, 588:1008]
            self.__image_width = 420
            self.__image_height = 50
        elif self.__slot_no == 'fourth':
            image = image[1290:1340, 588:1008]
            self.__image_width = 420
            self.__image_height = 50

        image = tf.cast(image, tf.float32) / 255.0

        return image

    def __training_loop(self, x_train, x_val, y_train_data, y_val_data):
        callbacks = [self.__early_stopping_for_loss, self.__reduce_lr_for_loss]
        self.__model.load_weights(self.__checkpoint_filepath)
        self.__model.fit(x_train, y_train_data,
                         validation_data=(x_val, y_val_data),
                         epochs=1000,
                         batch_size=self.__batch_size,
                         callbacks=callbacks)

    def __validation_loop(self, x_train, x_val, y_train_data, y_val_data):
        self.__model.load_weights(self.__checkpoint_filepath)
        callbacks = [self.__early_stopping, self.__model_checkpoint_callback, self.__reduce_lr]
        while True:
            tf.random.set_seed(time.time())
            history = self.__model.fit(x_train, y_train_data,
                                       validation_data=(x_val, y_val_data),
                                       epochs=self.__epochs,
                                       batch_size=self.__batch_size,
                                       callbacks=callbacks)

            # Check if early stopping was triggered
            if self.__early_stopping.stopped_epoch > 0:
                print("Early stopping triggered.")
            if self.__current_lr <= self.__minimum_lr:
                print("Minimum learning rate reached. Starting training for loss.")

            # Evaluate against threshold
            min_val_loss = min(history.history['val_loss'])
            min_val_loss_index = history.history['val_loss'].index(min_val_loss)
            last_val_loss = history.history['val_loss'][-1]
            if min_val_loss < self.__target_loss and last_val_loss < self.__target_loss:
                print("Threshold met on validation. Now training for loss.")
                break
            else:
                print(f"Threshold not met, min val_loss: {min_val_loss}. Restarting training with reduced LR.")
                # Load the best model weights
                self.__model.load_weights(self.__checkpoint_filepath)
                # Reduce learning rate
                self.__current_lr *= self.__current_lr_decay
                tf.keras.backend.set_value(self.__model.optimizer.lr, self.__current_lr)
        return history, min_val_loss_index

    def __test(self, x_test, y_test_data):
        test_results = self.__model.evaluate(x_test, {
            'output': y_test_data
        })
        print(f'Test results - Loss: {test_results[0]} - Accuracy: {test_results[1]}')
        # Make predictions on the whole dataset
        start_time = time.time()
        predictions = self.__model.predict(self.__x_data)
        predicted_labels = np.argmax(predictions, axis=-1)
        end_time = time.time()
        time_taken = end_time - start_time
        print(f"Time taken for predictions: {time_taken} thirds for {len(self.__x_data)} images "
              f"(avg of {time_taken / len(self.__x_data)} s per image)")
        actual_labels = np.argmax(self.__y_data, axis=-1)
        # Identify misclassified instances
        misclassified_indices = np.where(predicted_labels != actual_labels)[0]
        # Visualize or list misclassified instances
        for index in misclassified_indices:
            file_name = list(self.__annotations.keys())[index]  # Assuming the order hasn't changed
            if 'validated' not in self.__annotations[file_name] or not self.__annotations[file_name]['validated']:
                continue

            if self.__prediction_type == 'type':
                actual = convert_int_to_type(actual_labels[index])
                predicted = convert_int_to_type(predicted_labels[index])
            else:
                actual = str(actual_labels[index] + 2)
                predicted = str(predicted_labels[index] + 2)

            print(f"File: {file_name}, Actual: {actual}, Predicted: {predicted}")

            # Optionally, visualize the misclassified image
            image_path = os.path.join('resources', f'{file_name}.png')
            image = plt.imread(image_path)
            plt.imshow(image)
            plt.title(f"Actual: {actual}, Predicted: {predicted}")
            plt.show()

    def __show_examples(self):
        # Show 16 random images from the training set in a 4x4 grid in matplotlib
        plt.figure(figsize=(10, 10))
        for i in range(16):
            plt.subplot(4, 4, i + 1)
            plt.imshow(self.__x_data[i])
            if self.__prediction_type == 'percent':
                plt.title(np.argmax(self.__y_data[i]) + 2)
            else:
                plt.title(convert_int_to_type(np.argmax(self.__y_data[i])))
            plt.axis('off')
        plt.show()

    def __fill_dataset(self):
        for file_name, data in self.__annotations.items():
            if 'validated' not in data or not data['validated']:
                continue
            self.__x_data.append(
                self.__load_and_preprocess_image(image_path=os.path.join('resources', f'{file_name}.png')))
            if self.__slot_no == 'global':
                if self.__prediction_type == 'percent':
                    value = data['global']['percent']
                else:
                    value = convert_type_to_int(data['global'][self.__prediction_type])
                self.__y_data.append(tf.keras.utils.to_categorical(value, num_classes=self.__num_types))
            else:
                if self.__prediction_type == 'percent':
                    value = int(data[f'{self.__slot_no}_slot'][self.__prediction_type]) - 2
                else:
                    value = convert_type_to_int(data[f'{self.__slot_no}_slot'][self.__prediction_type])
                self.__y_data.append(tf.keras.utils.to_categorical(value, num_classes=self.__num_types))

    def begin_training(self):

        if self.__example:
            self.__show_examples()

        self.__x_data = np.array(self.__x_data)
        self.__y_data = np.array(self.__y_data)

        # Split the data into train, validation, and test sets
        x_train, x_temp, y_train_data, y_temp_data = train_test_split(self.__x_data, self.__y_data, test_size=0.4,
                                                                      random_state=42)

        x_val, x_test, y_val_data, y_test_data = train_test_split(x_temp, y_temp_data, test_size=0.33, random_state=42)

        self.__model = self.__choose_best_initial_point(x_train, x_val, y_train_data, y_val_data)

        history, min_val_loss_index = self.__validation_loop(x_train, x_val, y_train_data, y_val_data)

        if history.history['val_accuracy'][min_val_loss_index] == 1.0 and history.history['accuracy'][
            min_val_loss_index] == 1.0:
            print("Model already at 100% accuracy. No need to train further.")
        else:
            self.__training_loop(x_train, x_val, y_train_data, y_val_data)

        self.__model.save(self.__checkpoint_filepath)

        self.__test(x_test, y_test_data)

    def __create_model(self, learning_rate=None):
        if learning_rate is None:
            learning_rate = self.__initial_lr
        tf.random.set_seed(time.time())
        input_layer = Input(shape=(self.__image_height, self.__image_width, self.__channels))
        if self.__prediction_type == 'type':
            x = Conv2D(16, (3, 3), activation='relu')(input_layer)
            x = MaxPooling2D((2, 2))(x)
            x = Dropout(0.05)(x)
            x = Conv2D(5, (3, 3), activation='relu')(x)
            x = MaxPooling2D((2, 2))(x)
            x = Flatten()(x)
        else:
            x = Conv2D(16, (3, 3), activation='relu')(input_layer)
            x = MaxPooling2D((2, 2))(x)
            x = Dropout(0.05)(x)
            x = Conv2D(self.__num_types, (3, 3), activation='relu')(x)
            x = MaxPooling2D((2, 2))(x)
            x = Flatten()(x)
        output = Dense(self.__num_types, activation='softmax', name='output')(x)

        model = Model(inputs=input_layer, outputs=[
            output
        ])

        model.compile(optimizer=Adam(learning_rate=learning_rate),
                      loss={
                          'output': 'categorical_crossentropy'
                      },
                      metrics={
                          'output': 'accuracy'
                      })

        return model

    def __choose_best_initial_point(self, x_train, x_val, y_train_data, y_val_data):
        best_model = self.__model
        history = best_model.fit(x_train, y_train_data,
                                 validation_data=(x_val, y_val_data),
                                 epochs=self.__searching_points_epochs,
                                 batch_size=self.__batch_size,
                                 callbacks=[self.__early_stopping_for_searching]
                                 )
        best_model.save(self.__checkpoint_filepath)
        best_val_loss = min(history.history['val_loss'])
        patience = 0
        no_starting_points = 0
        while True:
            model = self.__create_model()
            history = model.fit(x_train, y_train_data,
                                validation_data=(x_val, y_val_data),
                                epochs=self.__searching_points_epochs,
                                batch_size=self.__batch_size,
                                callbacks=[self.__early_stopping_for_searching]
                                )
            min_val_loss = min(history.history['val_loss'])
            if min_val_loss < best_val_loss:
                best_model = model
                best_val_loss = min_val_loss
                best_model.save(self.__checkpoint_filepath)
                patience = 0

            no_starting_points += 1
            print()
            print("=" * 50)
            print("=" * 50)
            print("=" * 50)
            print(f"{no_starting_points} points evaluated. Patience remaining = {self.__searching_points_patience - patience}")
            print(f"Best val_loss so far: {best_val_loss}")
            print("=" * 50)
            print("=" * 50)
            print("=" * 50)
            print()
            if max(history.history['val_accuracy']) == 1.0:
                print()
                print("Model already at 100% accuracy on validation data. No need to search further.")
                print()
                break
            patience += 1
            if patience >= self.__searching_points_patience:
                print()
                print(f"No improvement in val_loss for {self.__searching_points_patience} tries. Stopping search.")
                print()
                break

        return best_model


def main():
    trainer = Trainer('global', 'type', example=False)
    trainer.begin_training()


if __name__ == '__main__':
    main()
