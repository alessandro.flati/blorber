# Please read the [Wiki](https://gitlab.com/alessandro.flati/blorber/-/wikis/home)!

The [Wiki](https://gitlab.com/alessandro.flati/blorber/-/wikis/home) contains all the practical informations, split by section of interest. Please refer to that.

Enjoy!
