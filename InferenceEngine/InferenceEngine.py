import collections
import json
import pathlib

import cv2
import easyocr
import numpy as np
import tensorflow as tf
from tensorflow.keras.models import load_model

from Blorber.Exceptions import GlobalPercentExtractionError, InvalidGlobalPercentError
from Trainer.Training import convert_int_to_type


class InferenceEngine:
    def __init__(self, models_path_str='resources/models'):
        self.__reader = easyocr.Reader(['en'], model_storage_directory='resources/easyocr_models')
        models_path = pathlib.Path(models_path_str)
        self.__models = {
            'global_type': load_model(models_path / 'global_type.keras'),
            'first_slot_percent': load_model(models_path / 'first_slot_percent.keras'),
            'second_slot_percent': load_model(models_path / 'second_slot_percent.keras'),
            'third_slot_percent': load_model(models_path / 'third_slot_percent.keras'),
            'fourth_slot_percent': load_model(models_path / 'fourth_slot_percent.keras'),
            'first_slot_type': load_model(models_path / 'first_slot_type.keras'),
            'second_slot_type': load_model(models_path / 'second_slot_type.keras'),
            'third_slot_type': load_model(models_path / 'third_slot_type.keras'),
            'fourth_slot_type': load_model(models_path / 'fourth_slot_type.keras')
        }
        self.__min_max_percents = {}
        minmax_percents = json.load(open('resources/blorb_level_advancement.json'))["minmax_percents_for_level"]
        for i in range(len(minmax_percents)):
            self.__min_max_percents[i+1] = {
                'min': float(minmax_percents[i]['min']),
                'max': float(minmax_percents[i]['max'])
            }
        self.__blorb_level = int(json.load(open('stats.json'))["blorb_level"])

    @staticmethod
    def __preprocess_image(image, slot_no):
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        processed_image = tf.convert_to_tensor(image, dtype=tf.uint8)
        if slot_no == 'global':
            processed_image = processed_image[930:1000, 568:1008]
        elif slot_no == 'first':
            processed_image = processed_image[1050:1100, 588:1008]
        elif slot_no == 'second':
            processed_image = processed_image[1130:1180, 588:1008]
        elif slot_no == 'third':
            processed_image = processed_image[1210:1260, 588:1008]
        elif slot_no == 'fourth':
            processed_image = processed_image[1290:1340, 588:1008]
        processed_image = tf.cast(processed_image, tf.float32) / 255.0
        processed_image = tf.expand_dims(processed_image, axis=0)
        return processed_image

    def infer(self, image):
        processed_image_global = self.__preprocess_image(image, 'global')
        processed_image_first_slot = self.__preprocess_image(image, 'first')
        processed_image_second_slot = self.__preprocess_image(image, 'second')
        processed_image_third_slot = self.__preprocess_image(image, 'third')
        processed_image_fourth_slot = self.__preprocess_image(image, 'fourth')

        first_slot_percent = self.__models['first_slot_percent'].predict(processed_image_first_slot, verbose=False)
        second_slot_percent = self.__models['second_slot_percent'].predict(processed_image_second_slot, verbose=False)
        third_slot_percent = self.__models['third_slot_percent'].predict(processed_image_third_slot, verbose=False)
        fourth_slot_percent = self.__models['fourth_slot_percent'].predict(processed_image_fourth_slot, verbose=False)
        first_slot_type = self.__models['first_slot_type'].predict(processed_image_first_slot, verbose=False)
        second_slot_type = self.__models['second_slot_type'].predict(processed_image_second_slot, verbose=False)
        third_slot_type = self.__models['third_slot_type'].predict(processed_image_third_slot, verbose=False)
        fourth_slot_type = self.__models['fourth_slot_type'].predict(processed_image_fourth_slot, verbose=False)

        global_type = self.__models['global_type'].predict(processed_image_global, verbose=False)
        global_type = convert_int_to_type(np.argmax(global_type))
        global_percent = self.__extract_global_percent(image, global_type)

        return {
            'global': {
                'type': global_type,
                'percent': global_percent,
            },
            'first_slot': {
                'type': convert_int_to_type(np.argmax(first_slot_type)),
                'percent': int(np.argmax(first_slot_percent)) + 2,
            },
            'second_slot': {
                'type': convert_int_to_type(np.argmax(second_slot_type)),
                'percent': int(np.argmax(second_slot_percent)) + 2,
            },
            'third_slot': {
                'type': convert_int_to_type(np.argmax(third_slot_type)),
                'percent': int(np.argmax(third_slot_percent)) + 2,
            },
            'fourth_slot': {
                'type': convert_int_to_type(np.argmax(fourth_slot_type)),
                'percent': int(np.argmax(fourth_slot_percent)) + 2,
            }
        }

    def __extract_global_percent(self, image, blorb_type):
        trials = {}
        for i in range(10):
            try:
                number = self.__extract_global_percent_trial(image, blorb_type, i)
                try:
                    self.__check_validity_of_global_percent(number)
                except InvalidGlobalPercentError:
                    number = self.try_alternatives(number)
                trials[i] = number
            except GlobalPercentExtractionError:
                continue
            except InvalidGlobalPercentError:
                continue
        if len(trials) == 0:
            raise GlobalPercentExtractionError

        occurrences = collections.Counter(trials.values())

        # Get the number that appears the most
        global_percent, _ = occurrences.most_common(1)[0]
        return global_percent

    def try_alternatives(self, number):
        found_alternative = False
        new_number = number
        for i, digit in enumerate(str(number)):
            if digit == '3':  # Replace the digit 3 with 8
                new_number = int(str(number)[:i] + '8' + str(number)[i + 1:])
                try:
                    self.__check_validity_of_global_percent(new_number)
                    found_alternative = True
                    break
                except InvalidGlobalPercentError:
                    continue
            elif digit == '8':  # Replace the digit 8 with 3
                new_number = int(str(number)[:i] + '3' + str(number)[i + 1:])
                try:
                    self.__check_validity_of_global_percent(new_number)
                    found_alternative = True
                    break
                except InvalidGlobalPercentError:
                    continue
            elif i == len(str(number)) - 1 and digit == '7':
                new_number = int(str(number)[:i])
                try:
                    self.__check_validity_of_global_percent(new_number)
                    found_alternative = True
                    break
                except InvalidGlobalPercentError:
                    continue
        if not found_alternative:
            raise InvalidGlobalPercentError(f"Could not find an alternative for {number}%")
        return new_number

    def __extract_global_percent_trial(self, image, blorb_type, trial):
        region = [
            None,
            image.shape[0] * 473 // 960,
            image.shape[1] * 35 // 270,
            image.shape[0] * 19 // 960
        ]
        if blorb_type == 'earth':
            region[0] = image.shape[1] * 193 // 270
        elif blorb_type == 'fire':
            region[0] = image.shape[1] * 192 // 270
        elif blorb_type == 'wind':
            region[0] = image.shape[1] * 194 // 270
        elif blorb_type == 'water':
            region[0] = image.shape[1] * 196 // 270
        center_x = region[0] + region[2] // 2
        x = center_x - region[2] // 2 + trial
        w = region[2] - 2 * trial
        y = region[1]
        h = region[3]
        global_percent_region = image[y:y + h, x:x + w]
        # Invert the colors to make the text black and the background white
        global_percent_region = cv2.bitwise_not(global_percent_region)

        extracted_text = self.__reader.readtext(global_percent_region)
        try:
            extracted_text = extracted_text[0][1]
        except IndexError:
            raise GlobalPercentExtractionError
        # Find the `+` sign and the `%` sign and extract the number between them
        extracted_text = extracted_text[extracted_text.find('+') + 1:extracted_text.find('%')]
        if len(extracted_text) == 0:
            raise GlobalPercentExtractionError
        try:
            global_percent = int(extracted_text)
        except ValueError:
            raise GlobalPercentExtractionError
        return global_percent

    def __check_validity_of_global_percent(self, number):
        min_possible_value = self.__min_max_percents[self.__blorb_level]['min'] - 5
        max_possible_value = self.__min_max_percents[self.__blorb_level]['max'] + 5
        if not (min_possible_value < number < max_possible_value):
            raise InvalidGlobalPercentError(
                f"Extracted invalid top stat for blorb level {self.__blorb_level}: {number}% "
                f"(min: {int(min_possible_value)}%, max: {int(max_possible_value)}%)"
            )
